#include "Game.hpp"

#include <SDL2/SDL.h>

#ifdef __WIN32
#include "windows.h"
#endif

#ifdef __WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPSTR lpCmdLine, int cmdShow)
#else
int main(int argc, char **argv)
#endif // __WIN32
{
	SDL_Init(SDL_INIT_EVERYTHING);

	cw::Game game{600, 600};

	SDL_Quit();

	return 0;
}
