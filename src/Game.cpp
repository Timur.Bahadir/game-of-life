#include "Game.hpp"

#include <cstdint>

namespace cw
{

Game::Game(int const windowWidth, int const windowHeight)
	: renderer{windowWidth, windowHeight}, window_width{windowWidth}, window_height{windowHeight}, board{}
{
	renderer.draw(board);
	main_loop();
}

void Game::main_loop()
{
	while (!quit)
	{
		handle_input();

		if (is_auto_playing)
		{
			tick();
			SDL_Delay(tick_delay);
		}
	}
}

void Game::handle_input()
{
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_a:
				if (!a_pressed)
				{
					tick();
					a_pressed = true;
				}
				break;
			case SDLK_s:
				if (!s_pressed)
				{
					is_auto_playing = !is_auto_playing;
					s_pressed = true;
				}
				break;
			case SDLK_r:
				if (!r_pressed)
				{
					board.reset();
					renderer.draw(board);
					r_pressed = true;
				}
				break;
			case SDLK_q:
				if (tick_delay > 20)
				{
					tick_delay -= 10;
				}
				break;
			case SDLK_w:
				tick_delay += 10;
				break;
			}
			break;
		case SDL_KEYUP:
			switch (event.key.keysym.sym)
			{
			case SDLK_a:
				a_pressed = false;
				break;
			case SDLK_s:
				s_pressed = false;
				break;
			case SDLK_r:
				r_pressed = false;
				break;
			}
			break;
		case SDL_MOUSEBUTTONDOWN:
			auto const mouse_position{screen_to_board(event.motion.x, event.motion.y)};
			TileState newState;

			switch (event.button.button)
			{
			case SDL_BUTTON_LEFT:
				newState = TileState::Alive;
				break;
			case SDL_BUTTON_RIGHT:
				newState = TileState::Dead;
				break;
			}
			board.set(mouse_position.first, mouse_position.second, newState);
			renderer.draw(board);
			break;
		}
	}
}

void Game::tick()
{
	board.update();
	renderer.draw(board);
}

std::pair<uint8_t, uint8_t> Game::screen_to_board(int const sx, int const sy) const
{
	int board_position_x;
	int board_position_y;

	if (sx > 0 && sy > 0)
	{
		board_position_x = ((float)Board::Board_Width / (float)window_width) * sx;
		board_position_y = ((float)Board::Board_Height / (float)window_height) * sy;
	}

	return std::pair<uint8_t, uint8_t>{board_position_x, board_position_y};
}

} // namespace cw
