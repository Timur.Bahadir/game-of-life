#include "Renderer.hpp"

#include "Board.hpp"
#include "Game.hpp"

#include <iostream>

namespace cw
{

namespace
{
constexpr uint32_t Dead_Color{0x2C3E50};
constexpr uint32_t Alive_Color{0xE74C3C};
}

Renderer::Renderer(int const windowWidth, int const windowHeight)
	: window_width{windowWidth}, window_height{windowHeight}
{
	if (!(window = SDL_CreateWindow("Game of Life", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_OPENGL)))
	{
		return;
	}

	if (!(renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED)))
	{
		return;
	}

	if (!(texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, Board::Board_Width, Board::Board_Height)))
	{
		return;
	}

	pixels = new uint32_t[Board::Board_Width * Board::Board_Height];
	memset(pixels, 255, Board::Board_Width * Board::Board_Height * sizeof(uint32_t));
}

Renderer::~Renderer()
{
	delete[] pixels;

	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}

void Renderer::draw(Board const &board)
{
	for (uint32_t y{0}; y < Board::Board_Height; ++y)
	{
		for (uint32_t x{0}; x < Board::Board_Height; ++x)
		{
			uint32_t color;
			switch (board.at(x, y))
			{
			case TileState::Alive:
				color = Alive_Color;
				break;
			case TileState::Dead:
				color = Dead_Color;
				break;
			}
			pixels[y * Board::Board_Width + x] = color;
		}
	}

	SDL_UpdateTexture(texture, NULL, pixels, Board::Board_Width * sizeof(uint32_t));
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}

} // namespace cw
