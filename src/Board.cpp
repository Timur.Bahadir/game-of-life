#include "Board.hpp"

namespace cw
{

namespace
{

bool position_is_inside_board(uint8_t const x, uint8_t const y)
{
	return !(x < 0 || x >= Board::Board_Width || y < 0 || y >= Board::Board_Height);
}

} // namespace

Board::Board()
{
	reset();
}

Board::~Board()
{
}

TileState Board::at(uint8_t const x, uint8_t const y) const
{
	if (!position_is_inside_board(x, y))
	{
		return TileState::Dead;
	}
	return grid[y][x];
}

void Board::set(uint8_t const x, uint8_t const y, TileState const state)
{
	if (position_is_inside_board(x, y))
	{
		grid[y][x] = state;
	}
}

uint8_t Board::get_neighbor_count(uint8_t const x, uint8_t const y) const
{
	uint8_t count{0};

	if (at(x - 1, y - 1) == TileState::Alive)
		++count;
	if (at(x, y - 1) == TileState::Alive)
		++count;
	if (at(x + 1, y - 1) == TileState::Alive)
		++count;

	if (at(x - 1, y) == TileState::Alive)
		++count;
	if (at(x + 1, y) == TileState::Alive)
		++count;

	if (at(x - 1, y + 1) == TileState::Alive)
		++count;
	if (at(x, y + 1) == TileState::Alive)
		++count;
	if (at(x + 1, y + 1) == TileState::Alive)
		++count;

	return count;
}

void Board::update()
{
	Grid new_grid{grid};

	for (uint8_t x{0}; x < Board_Width; ++x)
	{
		for (uint8_t y{0}; y < Board_Height; ++y)
		{
			uint8_t const neighbor_count{get_neighbor_count(x, y)};
			if (grid[y][x] == TileState::Alive)
			{
				if (neighbor_count < 2 || neighbor_count > 3)
				{
					new_grid[y][x] = TileState::Dead;
				}
			}
			else
			{
				if (neighbor_count == 3)
				{
					new_grid[y][x] = TileState::Alive;
				}
			}
		}
	}

	grid = new_grid;
}

void Board::reset()
{
	for (Row &row : grid)
	{
		for (TileState &cell : row)
		{
			cell = TileState::Dead;
		}
	}
}

} // namespace cw
