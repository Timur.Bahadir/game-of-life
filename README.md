# Game of Life

A basic game of life implementation using modern c++
and SDL for rendering.

## Building

Requirements:

- Installed CMake version 3.10
- Installed SDL2 library (booth development and runtime)

To build using CMake run:

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

## Controls

- Left Mouse: Set cell to alive
- Right Mouse: Set cell to dead
- A Key: Advance one tick
- S Key: Start/Stop simulation
- R Key: Reset cells
- Q Key: Speed up simulation
- W Key: Slow down simulation

## Video

![Demo Video](media/demo.webm)
