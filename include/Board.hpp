#pragma once

#include <array>

namespace cw
{

enum class TileState : uint8_t
{
	Alive,
	Dead,
};

class Board
{
public:
	constexpr static uint8_t Board_Width{100}, Board_Height{100};

	Board();
	~Board();

	TileState at(uint8_t const x, uint8_t const y) const;
	void set(uint8_t const x, uint8_t const y, TileState const state);

	uint8_t get_neighbor_count(uint8_t const x, uint8_t const y) const;

	void update();
	void reset();

private:
	using Row = std::array<TileState, Board_Height>;
	using Grid = std::array<Row, Board_Height>;

	Grid grid;
};

} // namespace cw
