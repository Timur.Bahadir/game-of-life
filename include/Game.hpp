#pragma once

#include "Board.hpp"
#include "Renderer.hpp"

#include <tuple>

namespace cw
{

class Game
{
public:
	Game(int const windowWidth, int const windowHeight);

private:
	void main_loop();

	void handle_input();
	void tick();

	std::pair<uint8_t, uint8_t> screen_to_board(int const sx, int const sy) const;

	bool quit{false};

	cw::Renderer renderer;
	int const window_width;
	int const window_height;

	cw::Board board;

	SDL_Event event;
	bool a_pressed{false}, s_pressed{false}, r_pressed{false};
	bool is_auto_playing{false};
	uint32_t tick_delay{100};
};

} // namespace cw
