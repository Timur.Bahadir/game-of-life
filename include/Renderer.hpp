#pragma once

#include <SDL2/SDL.h>

namespace cw
{

class Board;

class Renderer
{
public:
	Renderer(int const windowWidth, int const windowHeight);
	~Renderer();

	void draw(Board const &board);

private:
	int window_width, window_height;

	SDL_Window *window;
	SDL_Texture *texture;
	SDL_Renderer *renderer;

	uint32_t *pixels;
};

} // namespace cw
